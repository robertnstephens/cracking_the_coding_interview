#! /usr/bin/env python3

# given M*N matrix, each row and column sorted ascending, func to find element

'''
if val < start of column
    c -= 1
if val > end of column
    c += 1
if val < start of row
    r -= 1
if val > end of row
    r += 1

can distill down to two - compare with end of col + start of row or vice versa
'''

def search_matrix(M, val):
    r = 0
    c = len(M[0])-1

    while r < len(M) and c > -1:
        if val == M[r][c]:
            return (r, c)
        elif val < M[r][c]: 
            c -= 1     
        else:
            r += 1 

    return (None, None)


'''
if val < M[rm][cm] value if exists in top left quarter

if val > M[rm+1][cm+1] value if exists in bottom right quarter

else either in top right or bottom left
'''

def search_matrix_rec(M, rs, re, cs, ce, val):

    if re-rs <= 0 or ce-cs <= 0:
        if val == M[re][ce]:
            return (re, ce)
        else:
            return (None, None)

    rm = (rs+re)//2
    cm = (cs+ce)//2

    if val == M[rm][cm]:
        return (rm, cm)

    if val < M[rm][cm]:
        return search_matrix_rec(M, rs, rm, cs, cm, val)

    elif val > M[rm+1][cm+1]:
        return search_matrix_rec(M, rm+1, re, cm+1, ce, val)
    else:
        res = search_matrix_rec(M, rm, re, cs, cm, val)

        if res == (None, None):
            return search_matrix_rec(M, rs, rm, cm, ce, val)
        else:
            return res

def search_matrix_wrap_rec(M, val):
    return search_matrix_rec(M, 0, len(M)-1, 0, len(M[0])-1, val)

M = [[10, 30, 50, 70],
     [11, 31, 51, 71],
     [12, 32, 52, 72],
     [13, 33, 53, 73],
     [14, 34, 54, 74],
     [15, 35, 55, 75]]

for row in M:
    print(row)

val = 51
res = search_matrix(M, val)
print(val, res)

val = 40
res = search_matrix(M, val)
print(val, res)


val = 51
res = search_matrix_wrap_rec(M, val)
print(val, res)

val = 40
res = search_matrix_wrap_rec(M, val)
print(val, res)

val = 10
res = search_matrix_wrap_rec(M, val)
print(val, res)

val = 75
res = search_matrix_wrap_rec(M, val)
print(val, res)


