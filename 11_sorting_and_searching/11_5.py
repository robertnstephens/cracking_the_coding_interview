#! /usr/bin/env python3

# sorted array of strings interspersed with empty strings, find location of given string

'''
eg input:
"ball"
{"at", "", "", "", "ball", "", "", "car", "", "", "dad", "", ""}
output: 4
'''

# creating list of number of spaces per word, O(n)!!!!!
# versus binary search, which would be O(nlogn)
# use binary search with mid_left and mid_right, which move past spaces?

def bin_search_spaces(A, low, high, val):
    if low > high:
        return None

    mid_left = mid_right = (low+high)//2

    while A[mid_left] == "" and mid_left > 0:
        mid_left -= 1
        if A[mid_left] == val:
            return mid_left

    while A[mid_right] == "" and mid_right < len(A)-1:
        mid_right += 1
        if A[mid_right] == val:
            return mid_right

    if val < A[mid_left]:
        return bin_search_spaces(A, low, mid_left-1, val)
    
    if val > A[mid_right]:
        return bin_search_spaces(A, mid_right+1, high, val)

A = ["at", "", "", "", "ball", "", "", "car", "", "", "dad", "", ""]
val = "ball"
res = bin_search_spaces(A, 0, len(A)-1, val)
print("index should be: {}, result is {}. (val is {})".format(A.index(val), res, A[res]))


