#! /usr/bin/env python3

# given a sorted array of n ints that has been rotated and unknown number of times,
# write func to find element in array

# eg find 5 in:
# [15, 16, 19, 20, 25, 1, 3, 4, 5, 7, 10, 14]
# output 8 (index of 5)


'''

just do binary search but with an offset

'''


def bin_search_offset_rec(A, low, high, val, offset):
    if low > high:
        #import pdb; pdb.set_trace() 
        
        return None

    mid = (low+high)//2

    # [15, 16, 19, 20, 25, 1, 3, 4, 5, 7, 10, 14]
    # offset == 5
    # A[5] should mean A[0]
    # A[4] should mean A[-1] == A[len(A)-1] == A[11]

    mid_offset = calc_offset(A, mid, offset)

    if A[mid_offset] == val:
        return mid_offset
    elif A[mid_offset] < val:
        return bin_search_offset_rec(A, mid+1, high, val, offset)
    else:
        return bin_search_offset_rec(A, low, mid-1, val, offset)

def calc_offset(A, idx, offset):
    if idx < offset:
        idx_offset = len(A) - (offset-idx)
    else:
        idx_offset = idx - offset
    return idx_offset

def bin_search_offset(A, val):
    offset = 0
    for i in range(1, len(A)):
        if A[i] < A[i-1]:
            offset = i
            break
    return bin_search_offset_rec(A, 0, len(A)-1, val, offset)


A = [15, 16, 19, 20, 25, 1, 3, 4, 5, 7, 10, 14]

'''
val = 5
idx = bin_search_offset(A, val) 
print("find", val, ": idx should be 8:", idx)

val = 16
idx = bin_search_offset(A, val) 
print("find", val, ": idx should be 1:", idx)

val = 20
idx = bin_search_offset(A, val) 
print("find", val, ": idx should be 3:", idx)
'''

# TODO FAILS!
val = 25
idx = bin_search_offset(A, val)
print("find", val, ": idx should be 4:", idx)





def bin_search(A, low, high, val):
    if low > high:
        return None

    mid = (low+high)//2

    if A[mid] == val:
        return mid
    elif A[mid] < val:
        return bin_search(A, mid+1, high, val)
    else:
        return bin_search(A, low, mid-1, val)
B = [1, 3, 4, 5, 7, 10, 14, 15, 16, 19, 20, 25]


for i, b in enumerate(B):
    print(i, bin_search(B, 0, len(B)-1, b))

