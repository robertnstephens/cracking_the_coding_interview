#! /usr/bin/env python3

# find largest possible number of blocks in tower
# each block has to be smaller and lighter than one directly underneath

'''
eg 
input:
[(65, 100), (70, 150), (56, 90), (75, 190), (60, 95), (68, 11)]

output:
[(56, 90), (60, 95), (65, 100), (68, 110), (70, 150), (75, 190)]
'''

import random, unittest

# sort either by size or weight, THEN LARGEST DECREASING SUBARRAY PROBLEM !!!


########################################################
# wikipedia longest increasing subsequences, O(nlogn): #
########################################################

def longest_increasing_subsequence(X):
    """
    Find and return longest increasing subsequence of S.
    If multiple increasing subsequences exist, the one that ends
    with the smallest value is preferred, and if multiple
    occurrences of that value can end the sequence, then the
    earliest occurrence is preferred.
    """
    n = len(X)
    X = [None] + X  # Pad sequence so that it starts at X[1]
    M = [None]*(n+1)  # Allocate arrays for M and P
    P = [None]*(n+1)
    L = 0
    for i in range(1,n+1):
        if L == 0 or X[M[1]] >= X[i]:
            # there is no j s.t. X[M[j]] < X[i]]
            j = 0
        else:
            # binary search for the largest j s.t. X[M[j]] < X[i]]
            lo = 1      # largest value known to be <= j
            hi = L+1    # smallest value known to be > j
            while lo < hi - 1:
                mid = (lo + hi)//2
                if X[M[mid]] < X[i]:
                    lo = mid
                else:
                    hi = mid
            j = lo

        P[i] = M[j]
        if j == L or X[i] < X[M[j+1]]:
            M[j+1] = i
            L = max(L,j+1)

    # Backtrack to find the optimal sequence in reverse order
    output = []
    pos = M[L]
    while L > 0:
        output.append(X[pos])
        pos = P[pos]
        L -= 1

    output.reverse()
    return output

'''
# Try small lists and check that the correct subsequences are generated.

class LISTest(unittest.TestCase):
    def testLIS(self):
        self.assertEqual(longest_increasing_subsequence([]),[])
        self.assertEqual(longest_increasing_subsequence(list(range(10,0,-1))),[1])
        self.assertEqual(longest_increasing_subsequence(list(range(10))),list(range(10)))
        self.assertEqual(longest_increasing_subsequence([3,1,4,1,5,9,2,6,5,3,5,8,9,7,9]), [1,2,3,5,8,9])

unittest.main()
'''


###############################################################################
# Roughtly book's solution: recursive longest increasing subsequence, O(n^2): #
###############################################################################


def best_sequence(seq1, seq2):
    if len(seq1) > len(seq2):
        return seq1
    else:
        return seq2

def longest_inc_sub(A):
    A.sort() # sort by index 0 (i.e. size) 
    
    solutions = [[]]
    cur_index = 0
    longest_inc_sub_rec(A, solutions, cur_index)

    best_soln = []
    for soln in solutions:
        best_soln = best_sequence(best_soln, soln)

    return best_soln
    
def longest_inc_sub_rec(A, solutions, cur_index):
    if cur_index >= len(A):
        return 

    new_soln = []
    for soln in solutions:
        if len(soln) and soln[-1][1] < A[cur_index][1]: # compare by index 1 (i.e. weight)
            new_soln = best_sequence(new_soln, soln)
    
    new_soln.append(A[cur_index])
    solutions.append(new_soln)

    longest_inc_sub_rec(A, solutions, cur_index+1)


import random

A = [(random.randint(50, 70), random.randint(1, 10)) for _ in range(15)]
A.sort()
print(A)

print("solution 1:")
res = longest_increasing_subsequence([a[1] for a in A])
print(res)

print("solution 2:")
res = longest_inc_sub(A)
print(res)


