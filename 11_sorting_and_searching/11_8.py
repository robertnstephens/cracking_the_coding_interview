#! /usr/bin/env python3

# reading stream of ints, want to be able to periodically look up rank of x (number vals <= x)
# call track(x) for each generated number
# get_rank_of(x), rank not including x itself


'''
eg 
input:
5, 1, 4, 4, 5, 9, 7, 13, 3


get_rank_of(1) == 0
get_rank_of(3) == 1
get_rank_of(4) == 3

'''

class Node():

    def __init__(self, val, rank):
        self.val = val
        self.left, self.right = None, None
        self.rank = rank

    def add(self, val, rank):
        if val <= self.val:
            self.rank += 1
            if self.left == None:
                self.left = Node(val, rank)
            else:
                self.left.add(val, rank)
        else:
            if self.right == None:
                self.right = Node(val, rank)
            else:
                self.right.add(val, rank)

    def get_rank(self, val):
        if val == self.val:
            
            ''' 
            if self.left == None or self.left.val < val:
                return self.rank  + 1
            else:
                return self.left.get_rank(val)
            '''
            return self.rank + 1

        elif val < self.val:
            return self.left.get_rank(val)
        else:
            return self.rank + 1 + self.right.get_rank(val) 

    def in_order(self):
        if self.left != None:
            self.left.in_order()
        
        print(self.val, self.rank)

        if self.right != None:
            self.right.in_order()

A = [5, 1, 4, 4, 5, 9, 7, 13, 3]

root = Node(A[0], 0)
for i in range(1, len(A)):
    root.add(A[i], 0)

print('in order:')
root.in_order()

A.sort()
print(A)
for i, a in enumerate(A):
    print("#"*25) 
    print('rank of {} currently: {}'.format(a, root.get_rank(a)))
    print('rank of {} should be: {}'.format(a, i+1))
    
    print("#"*25) 
