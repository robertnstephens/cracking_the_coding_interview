#! /usr/bin/env python3

# write method to sort an array of strings so that all anagrams are next to each other



# problem doesn't call for *any* order aside from this:
# create hashtable -> list, alphabetized anagrams as keys
# then convert back into list

import random

words = ["care", "race", "acre", "who", "how", "lode", "dole", "silent", "listen", "amp", "map"]
random.shuffle(words)
print(words)

table = {}

for word in words:
    kl = list(word)
    kl.sort() 
    k = ''.join(kl) 
    if table.get(k) == None:
        table[k] = []
    table[k].append(word)

output_list = []
for k in table:
    output_list += table[k]
print(output_list)
