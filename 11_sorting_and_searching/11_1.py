#! /usr/bin/env python3

# you are given two sorted arrays, A and B
# A has a large enough buffer at end to hold B
# write method to merge B into A in sorted order


print("merges in O(n) time:")

def merge_arrays(A, B):
    k = len(A)-1
    i = len(A)-len(B)-1
    j = len(B)-1
    
    while i > -1 and j > -1:
        if B[j] > A[i]:
            A[k] = B[j]
            j -= 1
        else:
            A[k] = A[i]
            i -= 1
        k -= 1

    A[:j+1] = B[:j+1]

print("#"*100)
print("all of A is greater than B")
import random
a_len = 30
b_len = 10
A = random.sample(range(100, 140), a_len-b_len)
A.sort()
print(A)
A_end = [-1 for _ in range(b_len)]
A += A_end
B = random.sample(range(40), b_len)
B.sort()
print(B)

merge_arrays(A, B)
print(A)

print("#"*100)
print("A and B genuinely merge")
a_len = 30
b_len = 10
A = random.sample(range(40), a_len-b_len)
A.sort()
print(A)
A_end = [-1 for _ in range(b_len)]
A += A_end
B = random.sample(range(40), b_len)
B.sort()
print(B)

merge_arrays(A, B)
print(A)


print("#"*100)
print("all of B greater than A")
a_len = 30
b_len = 10
A = random.sample(range(40), a_len-b_len)
A.sort()
print(A)
A_end = [-1 for _ in range(b_len)]
A += A_end
B = random.sample(range(100, 140), b_len)
B.sort()
print(B)

merge_arrays(A, B)
print(A)


