#! /usr/bin/env python3

# N*N matrix of positive and negative values
# find submatrix with largest possible sum


import random

N = 4


# this is an O(N^3) solution 
# specifically O(R^2 C)

def get_max_submatrix_opt(M):

   
    '''
    for all combinations of rows, which column should be selected for max sum
    preprocess such that M[r][c] if sum of cth column from row 0 up to row r
    then basically use Kadane's algorithm
    '''

    PP = [[0 for _ in range(len(M[0])) ] for _ in range(len(M))]
    PP[0] = M[0]

    for r in range(1, len(M)):
        for c in range(0, len(M[0])):
            PP[r][c] = M[r][c] + PP[r-1][c]

    max_overall = 0

    # start row, end row, start column, end column
    max_idxs = (None, None, None, None)

    for sr in range(-1, len(M)-1):
        for er in range(sr+1, len(M)):
            max_so_far = 0
           
            # start row, end row, start column
            cur_idxs = (sr+1, er+1, 0)
            
            for c in range(0, len(M[0])):

                if sr == -1:
                    col_elem = PP[er][c]
                else:
                    col_elem = PP[er][c]-PP[sr][c]

                if max_so_far + col_elem > 0:
                    max_so_far += col_elem
                else:
                    max_so_far = 0
                    cur_idxs = (sr+1, er+1, c+1)

                if max_so_far > max_overall:
                    max_idxs = cur_idxs + (c+1,)
                    max_overall = max_so_far

    return max_overall, max_idxs


# brute force solution is O(N^6)!!! - N^4 possible submatrices, naive computaton for each would be O(N^2) 
# but this is an O(N^4) solution if submatrix because summation is O(1)

def preprocess(matrix):
    prepro_matrix = [[0 for _ in row] for row in matrix]
    prepro_matrix[0][0] = matrix[0][0]

    for x in range(1, len(matrix)):
        prepro_matrix[x][0] = prepro_matrix[x-1][0] + matrix[x][0]
    
    for y in range(1, len(matrix[0])):
        prepro_matrix[0][y] = prepro_matrix[0][y-1] + matrix[0][y]
    
    for x in range(1, len(matrix)):
        for y in range(1, len(matrix[0])):
            prepro_matrix[x][y] = matrix[x][y] + prepro_matrix[x][y-1] + prepro_matrix[x-1][y] - prepro_matrix[x-1][y-1]

    return prepro_matrix


def get_max_submatrix(pm):

    max_subm_val = -1000*1000*1000
    max_subm_idxs = []

    for start_row in range(-1, len(pm)-1):
        for end_row in range(start_row+1, len(pm)): 
            for start_col in range(-1, len(pm[0])-1):
                for end_col in range(start_col+1, len(pm[0])): 
                   
                    if start_row > -1 and start_col > -1:    
                        cur_area = pm[end_row][end_col] - pm[start_row][end_col] - pm[end_row][start_col] + pm[start_row][start_col]
                    elif start_row == -1 and start_col > -1: 
                        cur_area = pm[end_row][end_col] - pm[end_row][start_col]
                    elif start_row > -1 and start_col == 1: 
                        cur_area = pm[end_row][end_col] - pm[start_row][end_col]
                    else: # both start_row and start_col are -1
                        cur_area = pm[end_row][end_col]
                    
                    #max_subm_val = max(max_subm_val, cur_area)

                    # change to start:one past convention
                    if cur_area > max_subm_val:
                        max_subm_idxs = [start_row+1, end_row+1, start_col+1, end_col+1]
                        max_subm_val = cur_area


    return max_subm_val, max_subm_idxs


for i in range(10):
    
    random.seed(i)
    print("random seed: {}".format(i))
    
    matrix = [ [random.randint(-10, 10) for _ in range(N)] for _ in range(N)]
    
    print("#"*25)
    for row in matrix:
        print(row)
    print("#"*25)
    
    print("sum entire matrix:", sum([sum(row) for row in matrix]) )
    
    prepro_matrix = preprocess(matrix)
    
    print("#"*25)
    for row in prepro_matrix:
        print(row)
    print("#"*25)
    
    max_subm_val, max_subm_idxs = get_max_submatrix(prepro_matrix)
    print(max_subm_val, max_subm_idxs)


    print("#"*25)
    max_subm_val, max_subm_idxs = get_max_submatrix_opt(matrix)
    print("O(N^3) max result:", max_subm_val, max_subm_idxs)
    print("#"*25)

