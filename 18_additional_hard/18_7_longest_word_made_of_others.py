#! /usr/bin/env python3

'''
given a list of words, find the longest word made up of other words in the list ?? presumable entirely made of ??

eg
input;
cat, banana, dog, nana, walk, walker, dogwalker
outpu:
dogwalker
'''

words = ["cat", "banana", "dog", "nana", "walk", "walker", "elephants", "dogwalker", "ba"]

words.sort() # python's sort is stable
words.sort(key=len, reverse=True)

# to find a word made of other words, has to have length >= shortest two words


def starts_with(cur_remain, words):

    remainders = []

    for w in words:
        if w == cur_remain[:len(w)]:
            remainders.append( cur_remain[len(w):] )
            
    return remainders     


def is_word_conglomerate(word, words):

    remainders = [word]

    while len(remainders):

        r = remainders.pop()
        nr = starts_with(r, words)
        remainders += nr

        for r in remainders:
            if len(r) == 0:
                words_made_of_others.append( word )
                return True

    return False


min_len = len(words[-1]) + len(words[-2])

words_made_of_others = []

for i, word in enumerate(words):
    print("processing:", word)

    if len(word) < min_len:
        break 

    if is_word_conglomerate( word, words[:i]+words[i+1:] ):
        words_made_of_others.append( word )

for w in words_made_of_others:
    print("found:", w)

words_made_of_others.sort(key=len, reverse=True)
print("longest:", words_made_of_others[0])



