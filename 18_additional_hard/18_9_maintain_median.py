#! /usr/bin/env python3

# maintain "live" median

# if use a max heap and min heap and average 
# O(nlogn) time

import heapq, random

# min heap to store largest half of values
# max_heap to store smallest half of values
# maintain either balanced of len(min_heap) = len(max_heap) + 1

# heapq is always min heap, so use negative values in max heap!


def add_x(x, min_heap, max_heap):
    # both empty
    if len(min_heap) == 0 and len(max_heap) == 0:
        heapq.heappush(min_heap, x)
        return x

    # balanced - inc min_heap size by 1
    if len(min_heap) == len(max_heap):
        if x < 0-max_heap[0]: 
            m = heapq.heappop(max_heap)
            heapq.heappush(max_heap, 0-x)
            heapq.heappush(min_heap, 0-m)
        else: 
            heapq.heappush(min_heap, x)
        return min_heap[0]

    # unbalanced - inc max_heap size by 1
    if len(min_heap) == len(max_heap) + 1:
        if x > min_heap[0]: 
            m = heapq.heappop(min_heap)
            heapq.heappush(min_heap, x)
            heapq.heappush(max_heap, 0-m)
        else: 
            heapq.heappush(max_heap, 0-x)
        return (min_heap[0] - max_heap[0])/2
    else:
        print("error: min_heap size {}, max_heap size {}".format(len(min_heap), len(max_heap)))
        return None        


min_heap, max_heap = [], []
heapq.heapify(min_heap)
heapq.heapify(max_heap)

val_history = []

for _ in range(20):
    x = random.randint(0, 10)
    cur_median = add_x(x, min_heap, max_heap)

    ### for testing ###
    val_history.append(x) 
    val_history.sort()

    L = len(val_history)
    mid = L//2
    #test_median

    if L % 2:
        test_median = val_history[mid] 
    else:
        test_median = (val_history[mid-1]+val_history[mid])/2

    print("next val: {}, live median: {}, test median: {}, vals: {}".format(x, cur_median, test_median, val_history))

