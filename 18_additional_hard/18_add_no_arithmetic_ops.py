#! /usr/bin/env python3

# add two numbers using no + or any arithmetic operators


def add_no_arithmetic_ops(m, n):

    mask = 0x1
    res, carry = [], 0

    while m or n:
        # 1, 1
        if (m & mask) and (n & mask):
            if carry:
                res.append(1)
            else:
                res.append(0)
                carry = 1
        # 0, 1
        if (m & mask) ^ (n & mask):
            if carry: 
                res.append(0)
            else:
                res.append(1)
        # 0, 0
        elif not (m & mask) and not (n & mask):
            if carry: 
                res.append(1)
                carry = 0
            else:
                res.append(0)

        m >>= 1
        n >>= 1

    if carry:
        res.append(1)

    res.reverse()
    s = "".join(str(c) for c in res)

    return int(s, 2)


m, n = 11, 37
import sys
if len(sys.argv) == 3:
    m, n = int(sys.argv[1]), int(sys.argv[2])

res = add_no_arithmetic_ops(m, n)
print("{} plus {} equals {}".format(m, n, res))
