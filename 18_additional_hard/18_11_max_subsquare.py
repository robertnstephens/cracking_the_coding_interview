#! /usr/bin/env python3

# matrix either black or white pixel
# find max subsquare such that all four borders are black

import random

nrows, ncols = 5, 5

#screen = [ [random.randint(0, 1) for _ in range(ncols)] for _ in range(nrows) ]

'''
screen = [ [0, 0, 0, 0, 0], 
           [1, 0, 0, 0, 1], 
           [1, 0, 0, 0, 1], 
           [1, 0, 1, 0, 1], 
           [1, 0, 0, 0, 1] ]
'''

screen = [ [0, 0, 1, 0, 0], 
           [1, 0, 0, 0, 0], 
           [1, 0, 0, 0, 0], 
           [1, 0, 0, 0, 0], 
           [1, 0, 0, 1, 1] ]


for row in screen:
    print(row)

class Box():

    def __init__(self, sr=0, sc=0, h=0, w=0):
        self.sr = sr
        self.sc = sc
        self.h = h
        self.w = w
        self.size = h*w


    def display(self, screen):
        print(self.sr, self.sc, self.h, self.w)

        for r in range(len(screen)):
            line = []

            for c in range(len(screen[0])):
                if r == self.sr or r == self.sr+self.h:
                    if self.sc <= c <= self.sc+self.w: 
                        line.append("X")            
                    else:
                        line.append(str(screen[r][c]))

                else:
                    if c == self.sc or c == self.sc+self.w:
                        if self.sr <= r <= self.sr+self.h: 
                            line.append("X")
                        else:
                            line.append(str(screen[r][c]))
                    else:
                        line.append(str(screen[r][c]))

            print(line)

def check_border_from_start(screen, start):

    sr, sc = start[0], start[1]
    max_height = len(screen)-sr
    max_width = len(screen[0])-sc
  
    max_box = Box()

    for height in range(1, max_height):
        for width in range(1, max_width):
            zero_border = True

            if sum( screen[sr][sc:sc+width] ) != 0:
                zero_border = False

            if sum( screen[sr+height][sc:sc+width] ) != 0:
                zero_border = False
            
            sum_sides = 0
            for i in range(height):
                sum_sides += screen[sr+i][sc]
                sum_sides += screen[sr+i][sc+width]

                if sum_sides != 0:
                    zero_border = False

            if zero_border and height*width > max_box.size:
                max_box = Box(sr, sc, height, width)
        
    return max_box

def run(screen):

    max_box = Box(0, 0, 0, 0)
    
    for r in range(nrows):
        for c in range(ncols):
    
            start_finish = (r, c)
            cur_box = check_border_from_start(screen, start_finish)        
   
            if cur_box.size > max_box.size:
                max_box = cur_box

    return max_box

max_box = run(screen)
max_box.display(screen)





