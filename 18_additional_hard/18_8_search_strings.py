#! /usr/bin/env python3

# string s and array of smaller strings T
# write method to search s for each t in T


# create suffix tree


class SuffixTreeNode():
    def __init__(self):
        self.children = {}
        self.indices = []

    def insert(self, s, i):

        self.indices.append(i)

        if s != None and len(s) > 0:
            key = s[0] 
            if self.children.get(key) != None:
                child = self.children.get(key)
            else:
                child = SuffixTreeNode()
                self.children[key] = child 

            child.insert(s[1:], i)

    def search(self, s):
        if s == None or len(s) == 0:
            return self.indices
        else:
            first = s[0]
            if first in self.children:
                remainder = s[1:]
                return self.children.get(first).search(remainder)
        
        return None

class SuffixTree():
    def __init__(self, s):
        self.root = SuffixTreeNode()
        for i in range(len(s)):
            suffix = s[i:] 
            self.root.insert(suffix, i)
  
    def search(self, s):
        return self.root.search(s)


def run():

    S = "bibs"

    suffix_tree = SuffixTree(S)

    T = ["bibs", "bib", "ibs", "bs", "not_present"]

    for t in T:
        res = suffix_tree.search(t)

        if res != None:
            print("found:", t)
            for i in res:
                print(S[i:])
        else:
            print("didn't find:", t)

run()
