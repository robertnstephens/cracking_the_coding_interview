#! /usr/bin/env python3

# write a function to randomly generate a set of m integers from an array of size n
# each element must have equal probability of being drawn

import random

def m_rand_ints_from_n(m, n):

    A = [i for i in range(n)]

    res = []
    for i in range(m):
        r = random.randint(i, n)
        A[i], A[r] = A[r], A[i]

    return A[:m]


m, n = 10, 52
res = m_rand_ints_from_n(m, n)
print(res)

