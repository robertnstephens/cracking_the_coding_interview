#! /usr/bin/env python3

def get_perms(S):

    permutations = []

    if len(S) == 0:
        permutations.append("")
        return permutations

    first_char = S[0]
    remainder = S[1:]

    words = get_perms(remainder)

    for word in words:
        for i in range(len(word)+1):
            permutations.append( word[0:i] + first_char + word[i:])
   
    return permutations



perms = get_perms("abc")

#print("perms:", perms)

for perm in perms:
    print(perm)


