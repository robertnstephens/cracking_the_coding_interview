#! /usr/bin/env python3


# implement function to check if binary tree is balanced:
# balanced here means heights of two subtrees of a node can only every differ by one


# propagate back -1 if not balanced, otherwise height
def get_height(cur):
    if cur == None:
        return 0 

    left_height = get_height(cur.left)
    if left_height == -1:
        return -1

    right_height = get_height(cur.right)
    if right_height == -1:
        return -1


    if abs(left_height - right_height) > 1:
        return -1
    else:
        return max(left_height, right_height) + 1

def is_balanced_better(cur):
    if get_height(cur) == -1:
        return False
    else:
        return True

class Node(object):
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

    # edge case flaw, see examples below
    def is_balanced(self, cur_node, depth):

        global max_depth
        global min_depth
        if depth == 1:
            max_depth, min_depth = -1, 100000

        # leaf node
        if cur_node.left == None and cur_node.right == None:
            min_depth = min(depth, min_depth)
            max_depth = max(depth, max_depth)

        if cur_node.left != None:
            self.is_balanced(cur_node.left, depth+1)

        if cur_node.right != None:
            self.is_balanced(cur_node.right, depth+1)

        if depth == 1:
            print("max_depth:", max_depth, "min_depth:", min_depth)
            # definition of balanced tree can only differ by as much as one
            return max_depth - min_depth <= 1 

print("balanced binary tree:")
head = Node(10)
head.left = Node(5)
head.left.left = Node(2)
head.left.right = Node(7)

head.right = Node(15)
head.right.left = Node(17)
head.right.right = Node(12)
head.right.right.right = Node(14)

print(head.is_balanced(head, 1))
print( is_balanced_better(head) )

print("unbalanced binary tree")
head = Node(10)
head.left = Node(5)
head.left.left = Node(2)
head.left.right = Node(7)

head.right = Node(15)
head.right.left = Node(17)
head.right.right = Node(12)

cur = head.right.right
cur.right = Node(20)
cur = cur.right
cur.right = Node(100)

print(head.is_balanced(head, 1))
print( is_balanced_better(head) )






head = Node(10)
head.right = Node(12)
head.right.right = Node(14)
head.right.right.right = Node(16)
head.right.right.right.right = Node(20)

print(head.is_balanced(head, 1))  # True!
print( is_balanced_better(head) ) # False!

head.left = Node(5)

print(head.is_balanced(head, 1))  # now false
print( is_balanced_better(head) ) # False!





