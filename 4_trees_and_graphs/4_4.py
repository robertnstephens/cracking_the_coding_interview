#! /usr/bin/env python3

# given bin tree, create a linked list at each depth D

class Node(object):
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

def traverse(cur, order, depth):
    if order == 0:
        print(cur.val) 
    if cur.right != None:
        traverse(cur.right, order, depth+1)
    if order == 1:
        print("val:", cur.val, "depth:", depth) 
    if cur.left != None:
        traverse(cur.left, order, depth+1)
    if order == 2:
        print(cur.val) 

def create_ll_depth(cur, cur_depth, ll):
    if cur.left != None:
        create_ll_depth(cur.left, cur_depth+1, ll)
    
    while cur_depth > len(ll)-1:
        ll.append([])

    ll[cur_depth].append(cur.val)

    if cur.right != None:
        create_ll_depth(cur.right, cur_depth+1, ll)

def bfs_create_ll_depth(root, ll_out):

    current = [root]

    while len(current):
        ll_out.append(current)
        
        parents = current
        current = []

        for parent in parents:
            if parent.left != None:
                current.append(parent.left)
            if parent.right != None:
                current.append(parent.right)

        

head = Node(3)
head.right = Node(5)
head.right.right = Node(6)
head.right.left = Node(4)
head.left = Node(1) 
head.left.right = Node(2)
head.left.left = Node(0)

pre_order, in_order, post_order = 0, 1, 2
traverse(head, in_order, 0)
print("#"*25)
print("depth first:")

LL = [[]]
create_ll_depth(head, 0, LL)

for l in LL:
    print(l)

print("#"*25)
print("breadth first:")
LL2 = []
bfs_create_ll_depth(head, LL2)
for ll2 in LL2:
    print( [l.val for l in ll2] )


