#! /usr/bin/env python3

# given a sorted array, create binary search tree with minimal height

class Node(object):
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

def create_minimal_bst_rec(A, start, end):
    if start > end:
        return None

    mid = (start+end)//2

    node = Node(A[mid])
    node.left = create_minimal_bst_rec(A, start, mid-1)
    node.right = create_minimal_bst_rec(A, mid+1, end)

    return node

def create_minimal_bst(A):
    return create_minimal_bst_rec(A, 0, len(A)-1)

def traverse_in_order(cur):
    if cur.left != None:
        traverse_in_order(cur.left)

    print(cur.val)

    if cur.right != None:
        traverse_in_order(cur.right)


nums = [i for i in range(15)]

root = create_minimal_bst(nums)
traverse_in_order(root)


