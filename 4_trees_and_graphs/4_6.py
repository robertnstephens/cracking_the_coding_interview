#! /usr/bin/env python3


# get next (in-order) node
# each node has a link to its parent

import sys

class Node(object):
    def __init__(self, val, parent):
        self.val = val
        self.left, self.right = None, None
        self.parent = parent


# if n has a right subtree, return leftmost node of this subtree
# else, keep moving up tree while parent is on the left
# handle case of rightmost node of entire tree (return null)


def left_most_child(cur_node):
    if cur_node == None:
        return None

    while cur_node.left != None:
        cur_node = cur_node.left
    return cur_node

def get_next_node(cur_node):
    if cur_node == None:
        return None

    if cur_node.parent == None or cur_node.right != None:
        return left_most_child(cur_node.right)
    else:
        while cur_node.parent and cur_node == cur_node.parent.right:
            cur_node = cur_node.parent
        return cur_node.parent
        


print("binary search tree:")
head = Node(10, None)
head.left = Node(5, head)
head.left.left = Node(2, head.left)
head.left.right = Node(7, head.left)

head.right = Node(15, head)
head.right.left = Node(12, head.right)
head.right.right = Node(17, head.right)

res = get_next_node(head)
print(head.val, res.val)

hlr = head.left.right
res = get_next_node(hlr)
print(hlr.val, res.val)

hrr = head.right.right
res = get_next_node(hrr)
if not res:
    print("no possible next node")
else:
    print(hrr.val, res.val)



