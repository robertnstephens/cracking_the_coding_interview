#! /usr/bin/env python3

# given digraph, find is there a path between two nodes

from collections import deque

from enum import Enum
class State(Enum):
    unvisited = 0
    visiting = 1
    visited = 2


def search(graph, start, end):
    graph.reset() 
  
    start.state = State.visiting

    queue = deque([start])

    while len(queue) > 0:
        cur = queue.popleft()
        cur_adj = cur.get_adjacent()

        for nn in cur_adj:
            if nn.state == State.unvisited:
                nn.state = State.visiting
                if nn == end:
                    return True
                queue.append(nn) 
    
        cur.state = State.visited

    return False


class Node(object):
    def __init__(self, val):
        self.val = val
        self.edges = []
        self.state = State.unvisited

    def add_edge(self, node):
        self.edges.append(node)

    def get_adjacent(self):
        return self.edges

class Graph(Node):
    def __init__(self, nodes):
        self.nodes = nodes
        for node in self.nodes:
            node.state = State.unvisited

    def reset(self):
        for node in self.nodes:
            node.state = State.unvisited


nodes = [Node(val) for val in range(10)]
for i in range(1, len(nodes)):
    nodes[i-1].add_edge(nodes[i])

res = search(Graph(nodes), nodes[2], nodes[7])
print("2->7 should be True:", res)

res = search(Graph(nodes), nodes[7], nodes[2])
print("7->2 should be False:", res)

nodes[7].add_edge(nodes[1])

res = search(Graph(nodes), nodes[7], nodes[2])
print("edge added, now 7->2 should be True:", res)

nodes = [Node(val) for val in range(10)]
# now don't connect last one!
for i in range(1, len(nodes)-1):
    nodes[i-1].add_edge(nodes[i])

nodes[6].add_edge(nodes[3])

res = search(Graph(nodes), nodes[2], nodes[-1])
print("should be False plus infinite loop is avoided:", res)

