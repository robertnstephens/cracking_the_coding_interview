#! /usr/bin/env python3


# find highest common ancestor of two nodes
# not neccessarily BST
# don't store nodes in additional data structure

import sys

class Node(object):
    def __init__(self, val, parent):
        self.val = val
        self.left, self.right = None, None
        # should ask whether we have parent in object
        # assume we don't
        # self.parent = parent
        # is we could use parent and was_visited, then solution O(logn) time


def is_descendent(root, poss_child):
    
    if root == None:
        return False
    if root == poss_child:
        return True
    
    return is_descendent(root.left, poss_child) or is_descendent(root.right, poss_child)

# O(n) - first called on 2n nodes, then 2n/2, then 2n/4, 2n/8
def highest_common_ancestor_rec(root, c1, c2):
    if root == c1 or root == c2:
        return root
    if root == None:
        return None

    c1_on_left = is_descendent(root.left, c1)
    c2_on_left = is_descendent(root.left, c2)

    if c1_on_left != c2_on_left:
        return root

    if c1_on_left:
        return highest_common_ancestor_rec(root.left, c1, c2)
    else:
        return highest_common_ancestor_rec(root.right, c1, c2)


def highest_common_ancestor(root, c1, c2):
    if not is_descendent(root, c1) or not is_descendent(root, c2):
        print("one or more not descendents of root at all!")
        return None

    return highest_common_ancestor_rec(root, c1, c2)



'''
still O(n) but constant factor faster - don't search nodes again, instead "bubble up"

if returns p, subtree includes p but not q
if returns q, subtree includes q but not p
if None, neither p nor q are in subtree
else, return common ancestor
'''

def highest_common_ancestor_faster_rec(root, c1, c2):

    if root == None:
        return None
    if root == c1 and root == c2:
        return root

    x = highest_common_ancestor_faster_rec(root.left, c1, c2)
    if x != None and x != c1 and x != c2:
        return x
    y = highest_common_ancestor_faster_rec(root.right, c1, c2)
    if y != None and y != c1 and y != c2:
        return y

    if x != None and y != None: # x and y found in different subtrees
        return root # so, root is common ancestor 
    elif root == c1 or root == c2:
        return root
    else:
        if x != None:
            return x
        if y != None:
            return y

def highest_common_ancestor_faster(root, c1, c2):
    if not is_descendent(root, c1) or not is_descendent(root, c2):
        print("one or more not descendents of root at all!")
        return None

    return highest_common_ancestor_faster_rec(root, c1, c2)


head = Node(10, None)
head.left = Node(5, head)
head.left.left = Node(2, head.left)
head.left.right = Node(7, head.left)

head.right = Node(15, head)
head.right.left = Node(12, head.right)
head.right.right = Node(17, head.right)


print("#"*25)
print("O(n) solution:")
res = highest_common_ancestor(head, head.left, head.left.right)
print(res.val)

res = highest_common_ancestor(head, head.right.left, head.right.right)
print(res.val)

res = highest_common_ancestor(head, head.left.left, head.right.right)
print(res.val)

print("#"*25)
print("faster constant O(n) solution:")
res = highest_common_ancestor_faster(head, head.left, head.left.right)
print(res.val)

res = highest_common_ancestor_faster(head, head.right.left, head.right.right)
print(res.val)

res = highest_common_ancestor_faster(head, head.left.left, head.right.right)
print(res.val)
print("#"*25)



