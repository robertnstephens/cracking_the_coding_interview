#! /usr/bin/env python3


# is binary tree a binary search tree?

import sys

class Node(object):
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

def is_bst(cur_node):
    global prev_val
   
    if cur_node == None:
        return True

    if not is_bst(cur_node.left):
        return False

    if prev_val > cur_node.val:
        return False
    
    prev_val = cur_node.val 

    if not is_bst(cur_node.right):
        return False

    return True


# second solution:
# if branch left, tighten max
# if branch right, tighten min

def is_bst_min_max(cnode, cmin, cmax):
    if cnode == None:
        return True

    if cnode.val <= cmin or cnode.val > cmax:
        return False

    if not is_bst_min_max(cnode.left, cmin, cnode.val) or not is_bst_min_max(cnode.right, cnode.val, cmax):
        return False

    return True

print("#"*25)
print("binary search tree:")
head = Node(10)
head.left = Node(5)
head.left.left = Node(2)
head.left.right = Node(7)

head.right = Node(15)
head.right.left = Node(12)
head.right.right = Node(17)

print("first method:")
prev_val = -sys.maxsize
res = is_bst(head)
print(res)

print("second method:")
res2 = is_bst_min_max(head, -sys.maxsize, sys.maxsize)
print(res2)

print("#"*25)
print("not a binary search tree:")
head = Node(10)
head.left = Node(16)
head.left.left = Node(2)
head.left.right = Node(7)

head.right = Node(15)
head.right.left = Node(17)
head.right.right = Node(12)
head.right.right.right = Node(14)

print("first method:")
prev_val = -sys.maxsize
res = is_bst(head)
print(res)


print("second method:")
res2 = is_bst_min_max(head, -sys.maxsize, sys.maxsize)
print(res2)

print("#"*25)
