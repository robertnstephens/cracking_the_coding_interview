#! /usr/bin/env python3

# design stack with not just push and pop with min function
# push, pop and min operate with O(1) time efficiency


class Stack(object):

    def __init__(self):
        self.stack = []
        self.cur_min = None

    def push(self, val):
        if self.cur_min == None:
            self.cur_min = val
        else:
            self.cur_min = min(val, self.cur_min)
        self.stack.append((val, self.cur_min))

    def pop(self):
        if len(self.stack) == 0:
            print("cannot pop from empty stack")
            return None
        val = self.stack[-1][0]
        del self.stack[-1]
        return val

    def min(self):
        if len(self.stack) == 0:
            print("no meaningful min element in empty stack")
            return None
        else:
            return self.stack[-1][1]
        


stack = Stack()

import random
vals = [random.randint(0, 20) for _ in range(10)]
for val in vals:
    print("push:", val) 
    stack.push(val)

for i in range(len(vals)):
    print("min:", stack.min(), "pop:", stack.pop())


