#! /usr/bin/env python3

# create new stack if old one exceeds capacity

class SetOfStacks(object):

    def __init__(self):
        self.stacks = [[]]
        self.size_limit = 5
        self.cur_stack = 0

    def push(self, val):

        if len(self.stacks[self.cur_stack]) >= self.size_limit:
            self.cur_stack += 1
            self.stacks.append([])
        
        self.stacks[self.cur_stack].append(val)

    def pop(self):
        val = self.stacks[self.cur_stack][-1]
        del self.stacks[self.cur_stack][-1]
      
        if len(self.stacks[self.cur_stack]) == 0 and self.cur_stack > 0:
            self.cur_stack -= 1

        return val

set_of_stacks = SetOfStacks()

import random
vals = [random.randint(0, 20) for _ in range(25)]
for val in vals:
    set_of_stacks.push(val)

for val in vals[::-1]:
    print(set_of_stacks.pop(), val)

