#! /usr/bin/env python3

# Tower of Hanoi - 3 towers, disks in descending order on one
# discs can only be place on larger ones 
# move discs from one tower to another


'''
1 discs
move source to dest

2 discs
move source to buffer
move source to dest
move buffer to dest

3 discs

two first, switching buffer and dest
    2 discs, switched
    move source to dest
    move source to buffer
    move dest to buffer
    
move source to dest
move buffer to source
move buffer to dest
move source to dest

'''


class TowerHanoi(object):

    def __init__(self, num_discs):
        self.num_discs = num_discs
        self.towers = [[d for d in range(self.num_discs, 0, -1)], [], []]

    def move(self, source, dest):

        if len(self.towers[source]) == 0:
            print("error, source empty")

        if len(self.towers[dest]self.towers[source][-1] > self.towers[dest][-1]:
            print("error, trying to move", self.towers[source][-1], "to", self.towers[dest][-1])
            return 

        disc = self.towers[source].pop()
        self.towers[dest].append(disc)

    def run(self, source, buff, dest):

        '''
        2 discs
        move source to buffer
        move source to dest
        move buffer to dest
        '''
        
        self.move(source, buff)
        self.move(source, dest)
        self.move(buff, dest)

        if len(self.towers[dest]) == self.num_discs:
            return self.towers[dest]


tower_hanoi = TowerHanoi(2)

source, buff, dest = 0, 1, 2
completed = tower_hanoi.run(source, buff, dest)

print(completed)










