#! /usr/bin/env python3


# sort a stack ascending
# push, pop, peek, is_empty
# can use other stacks, nothing else


import random

class StackSort(object):

    def __init__(self):
        stack0 = [random.randint(0, 20) for _ in range(10)]
        stack1, stack2 = [], []
        self.stacks = [stack0, stack1, stack2]

    def peek(self, num):
        return self.stacks[num][-1]

    def pop(self, num):
        val = self.stacks[num][-1]
        del self.stacks[num][-1]
        return val

    def push(self, num, val):
        val = self.stacks[num].append(val)

    def is_empty(self, num):
        return len(self.stacks[num]) == 0

    def run_sort(self):
        self.run_print(0)
        val = self.pop(0)
        self.push(1, val)
        
        while self.peek(0) < self.peek(1):
            self.push(1, self.pop(0)) 

        while not self.is_empty(0):
            self.push(2, self.pop(0))

            # remove portion of s1 which is smaller than s2 single value
            while not self.is_empty(1) and self.peek(1) < self.peek(2):
                self.push(0, self.pop(1))
    
            # add single s2 value to s1 in place
            self.push(1, self.pop(2)) 
    
            # then copy back that of s0 which is smaller
            while not self.is_empty(0) and self.peek(0) <= self.peek(1):
                self.push(1, self.pop(0)) 
    
        self.run_print(1)

    def run_print(self, num):

        print("#"*25)
        print(num, ":")
        print("#"*25)
        for s in self.stacks[num]:
            print(s)
        print("#"*25)

stack_sort = StackSort()

stack_sort.run_sort()




