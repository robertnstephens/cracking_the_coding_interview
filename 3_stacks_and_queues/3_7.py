#! /usr/bin/env python3

import random

'''
oldest either cat or dog or just oldest animal

deque_any, deque_dog, dequeue_cat

can using built in linked list
'''

class AnimalShelter(object):

    def __init__(self):
        self.dog_dates, self.cat_dates = [], []

    def deque_any(self):
        if len(self.dog_dates) and len(self.cat_dates): 
            if self.dog_dates[0] <= self.cat_dates[0]:
                return self.deque_dog()
            else:
                return self.deque_cat()

        elif len(self.dog_dates) or len(self.cat_dates): 
            if len(self.dog_dates) == 0:
                return self.deque_cat()
            elif len(self.cat_dates) == 0:
                return self.deque_dog()
        else:
            print("error, no animals left")
            return None

    def deque_dog(self):
        if len(self.dog_dates) == 0:
            print("error, no dogs left")
            return None

        date = self.dog_dates[0]
        del self.dog_dates[0]
        return date

    def deque_cat(self):
        if len(self.cat_dates) == 0:
            print("error, no cats left")
            return None

        date = self.cat_dates[0]
        del self.cat_dates[0]
        return date

    def enque_dog(self, date_admitted):
        self.dog_dates.append(date_admitted)

    def enque_cat(self, date_admitted):
        self.cat_dates.append(date_admitted)

animal_shelter = AnimalShelter()

num_dogs = 10
for date_admitted in range(num_dogs):
    animal_shelter.enque_dog(date_admitted)

num_cats = 5
for date_admitted in range(num_cats):
    animal_shelter.enque_dog(date_admitted)

for i in range(num_dogs+num_cats):
    rand = random.randint(0,2)
    if rand == 0:
        print("any", animal_shelter.deque_any())
    elif rand == 1:
        print("dog", animal_shelter.deque_dog())
    else:
        print("cat", animal_shelter.deque_cat())

