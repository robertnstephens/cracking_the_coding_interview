#! /usr/bin/env python3

# MyQueue class which implements a queue using two stacks

# create FIFO from two LIFO
class MyQueue(object):

    def __init__(self):
        self.stack0, self.stack1 = [], []
         
    def push(self, val):
        self.stack0.append(val)

    def pop(self):
        # only have to copy over when stack1 has "run out"
        if len(self.stack1) == 0:
            while len(self.stack0):
                self.stack1.append(self.stack0.pop())
        return self.stack1.pop()

    def is_empty(self):
        return len(self.stack0) == 0 and len(self.stack1) == 0


vals = [i for i in range(10)]

my_queue = MyQueue()

print("#"*25)
for val in vals:
    print(val)
    my_queue.push(val)
print("#"*25)
while not my_queue.is_empty():
    val = my_queue.pop()
    print(val)
print("#"*25)

