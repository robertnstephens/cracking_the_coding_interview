#! /usr/bin/env python3

# given a 2D graph with points on it, find the line which passes through most points


# =>> don't do brute force O(n^3) way, instead hash lines in the form of slopes and y-intercepts

class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Line(object):
    def __init__(self, m, m_infinite, intercept):
        self.m = m
        self.m_infinite = m_infinite
        self.intercept = intercept

        hash_precision = 1000000
        self.hash = self.run_hash(hash_precision)

    def run_hash(self, prec):
        if self.m_infinite:
            return int(self.intercept*prec) << 16
        else:
            return int(self.m*prec) << 16 | int(self.intercept*prec) << 8

def process_line(p, q, lines):
    epsilon = 1e-9

    if abs(p.x - q.x) < epsilon:
        slope = -1
        slope_infinite = True
        intercept = p.x
    else:
        slope = abs((p.y-q.y)/(q.x-p.x))
        slope_infinite = False
        intercept = p.y - slope*p.x # y = mx + b

    line = Line(slope, slope_infinite, intercept)
 
    #print(line.hash, p.x, p.y, q.x, q.y)

    if lines.get(line.hash) == None:
        lines[line.hash] = 1
    else:
        lines[line.hash] += 1

    return lines[line.hash], line

def get_best_line(points):

    lines = {}
    best_count, best_line = -1, None

    for i in range(len(points)):
        for j in range(1, len(points)):
            cur_count, cur_line = process_line(points[i], points[j], lines)

            if cur_count > best_count:
                best_count = cur_count
                best_line = cur_line 

    print("best line hash:", best_line.hash)
    return best_count, best_line

import random
X = [random.randint(-25, 25) for _ in range(100)]
Y = [random.randint(-25, 25) for _ in range(100)]

points = [Point(i, j) for i, j in zip(X, Y)]

best_count, best_line = get_best_line(points)

print("most points contained:", best_count)
print("slope:", best_line.m, "intercept:", best_line.intercept)

