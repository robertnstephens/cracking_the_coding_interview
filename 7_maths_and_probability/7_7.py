#! /usr/bin/env python3

# find kth number such that the only prime factors are 3, 5 and 7

from collections import deque



def get_kth_num(k):

    q3, q5, q7 = deque(), deque(), deque()
    
    q3.append(3)
    q5.append(5)
    q7.append(7)

    cur_val = 1

    for i in range(k):

        if q3[0] < q5[0] and q3[0] < q7[0]:

            cur_val = q3.popleft()
            q3.append(3*cur_val)
            q5.append(5*cur_val)
            q7.append(7*cur_val)

        elif q5[0] < q3[0] and q5[0] < q7[0]:

            cur_val = q5.popleft()
            q5.append(5*cur_val)
            q7.append(7*cur_val)

        elif q7[0] < q3[0] and q7[0] < q5[0]:

            cur_val = q7.popleft()
            q7.append(7*cur_val)

    return cur_val

k = 10
import sys
if len(sys.argv) == 2:
    k = int(sys.argv[1])

res = get_kth_num(k)
print("k:", k, "kth prod of 3,5,7:", res)

