#! /usr/bin/env python3

'''
replace all spaces in string with '%20'

I think it wants it done "in place" in string

'''

# the way I would do it
def run_replace(S):
    return ''.join([c if c != ' ' else "%20" for c in S])

# the way the book maybe wants to explore
def run_replace_book(S):
    for i in range(len(S)):
        if S[i] == ' ':
            S = S[:i] + "%20" + S[i+1:]
    return S

S1 = "is perm"
S2 = "prem is"
print(S1, run_replace(S1))
print(S2, run_replace(S2))

print(S1, run_replace_book(S1))
print(S2, run_replace_book(S2))
