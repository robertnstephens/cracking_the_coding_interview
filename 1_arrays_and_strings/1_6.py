#! /usr/bin/env python3

import copy

'''
rotate N*N matrix by 90 degrees 
can it be done in place?
'''


def rotate(M):
    N = len(M)
    for layer in range(N//2):
        first = layer
        last = N - 1 - layer

        for i in range(first, last):
            offset = i - first

            # save top
            top = M[first][i] 
            
            # left->top
            M[first][i] = M[last-offset][first]
            
            # bottom->left
            M[last-offset][first] = M[last][last-offset]
            
            # right->bottom
            M[last][last-offset] = M[i][last]

            # top->right
            M[i][last] = top

    return M


def rotate_not_in_place(M):
    C = copy.deepcopy(M)
    N = len(M)
    for i in range(N):
        for j in range(i, N-i):
            # top to right side
            C[j][N-i-1] = M[i][j] 
            # right side to bottom
            C[N-i-1][N-j-1] = M[j][N-i-1]
            # bottom to left side
            C[j][i] = M[N-i-1][j]
            # left side to top
            C[i][N-j-1] = M[j][i]
    return C

M = [[1,2,3],
     [4,5,6],
     [7,8,9]]

print("#"*20)
for m in M:
    print(m)
print("#"*20)
C = rotate(M)
print("#"*20)
for c in C:
    print(c)
print("#"*20)

M = [[1,2,3,4],
     [5,6,7,8],
     [9,10,11,12],
     [13,14,15,16]]

print("#"*20)
for m in M:
    print(m)
print("#"*20)
C = rotate(M)
print("#"*20)
for c in C:
    print(c)
print("#"*20)







