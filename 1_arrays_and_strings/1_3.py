#! /usr/bin/env python3

'''
Is one string a permutation of the other?

'''

# O( nlogn ) time
def is_perm(S1, S2):
    L1, L2 = list(S1), list(S2)
    L1.sort()
    L2.sort()
    return L1 == L2

# O(n) time??
def is_perm_better(S1, S2):
    if len(S1) != len(S2):
        return False

    D1 = {s:0 for s in S1}
    D2 = {s:0 for s in S2}

    for a, b in zip(S1, S2):
        D1[a] += 1
        D2[b] += 1

    for k in D1:
        if D1[k] != D2.get(k):
            return False

    return True

S1 = "is perm"
S2 = "prem is"
print(S1, S2, is_perm(S1, S2))

S1 = "is perm"
S2 = "not a permutation"
print(S1, S2, is_perm(S1, S2))


S1 = "is perm"
S2 = "prem is"
print(S1, S2, is_perm_better(S1, S2))

S1 = "is perm"
S2 = "not a permutation"
print(S1, S2, is_perm_better(S1, S2))


