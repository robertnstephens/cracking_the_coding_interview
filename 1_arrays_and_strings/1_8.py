#! /usr/bin/env python3

import copy

'''
assume have method, is_substring

check if s2 is a rotation of s1 using only one call to is_substring
'''

'''
if S2 is rotation == yx
S1 + S1 == xyxy - S2 will be a substring of this!
'''


def is_substring(S1, S2):
    return S2 in S1

def is_rotation(S1, S2):
    return is_substring(S1+S1, S2)

S1 = "waterbottle" # xy -> x == wat, y = erbottle
S2 = "erbottlewat" # yx
print(S1, S2, is_rotation(S1, S2))


S1 = "waterbottle" 
S2 = "erbottzewat" # not a rotation
print(S1, S2, is_rotation(S1, S2))
