#! /usr/bin/env python3

'''
Does a string have all unique characters?

What if you can't use additional data structures?

'''

def all_unique(S):
    char_counts = {s:0 for s in S}
    for s in S:
        char_counts[s] += 1
        if char_counts[s] > 1:
            return False
    return True

S1 = "uniqe"
S2 = "not a unique string"
print(S1, all_unique(S1))
print(S2, all_unique(S2))

def all_unique_no_additional_data_structures(S):
    char_array = [0 for _ in range(256)] 
    for s in S:
        char_array[ord(s)] += 1
        if char_array[ord(s)] > 1:
            return False
    return True

S1 = "uniqe"
S2 = "not a unique string"
print(S1, all_unique(S1))
print(S2, all_unique(S2))


