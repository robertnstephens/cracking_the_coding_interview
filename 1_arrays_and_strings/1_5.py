#! /usr/bin/env python3

'''
implement function to do basic string compression using counts
eg aabcccccaa becomes a2b1c5a3
if "compressed string not smaller return original string

'''


# TODO do without string concatenation, which is O(n^2)

# calc size, use list

def run_compress(S):

    R = ""
    i, j, count = 0, 0, 0

    while 1:
        if j > len(S)-1 or S[i] != S[j]:
            R += S[i] + str(count)
            i = j
            count = 0
            
            if j > len(S)-1:
                break
        else:
            j += 1
            count += 1

    if len(R) < len(S):
        return R
    else:
        return S

S1 = "aabcccccaa"
print(S1, run_compress(S1))

S2 = "abcdefgh"
print(S2, run_compress(S2))


