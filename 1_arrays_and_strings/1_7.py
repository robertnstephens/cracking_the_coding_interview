#! /usr/bin/env python3

import copy

'''
if M[i,j] == 0 row i col j all set to 0 also
'''

def set_zeros(M):
    C = copy.deepcopy(M)
    for r in range( len(M) ):
        for c in range( len(M[0]) ):
            if M[r][c] == 0:
                for i in range( len(M) ):
                    C[i][c] = 0
                for j in range( len(M[0]) ):
                    C[r][j] = 0

    return C

M = [[1,2,3],
     [0,5,6],
     [7,8,9]]

print("#"*20)
for m in M:
    print(m)
print("#"*20)
C = set_zeros(M)
print("#"*20)
for c in C:
    print(c)
print("#"*20)

M = [[1,2,3,4],
     [5,6,0,8],
     [9,10,11,12],
     [0,14,15,16]]

print("#"*20)
for m in M:
    print(m)
print("#"*20)
C = set_zeros(M)
print("#"*20)
for c in C:
    print(c)
print("#"*20)
