#include <iostream>
#include <cstdlib>

// implement a function void reverse(char* str) which reverses a null terminated string

using namespace std;

void run_reverse(char* str) {
    int start = 0;
    int end = 0;

    while(str[end] != '\0') {
        end++;
    }

    cout << start << endl; 
    cout << end << endl;

    int i = start;
    int j = end-1;
  
    for( ; end > start; i++, j--) {
        
        cout << i << " " << j << endl; 
        
        char temp = str[i];
       
        cout << str[i] << " " << str[j] << endl;

        // seg faults here
        str[i] = str[j];
        str[j] = temp;
    }

}

int main(void) {

    char* S1 = "abcde\0";
    //char* S1 = {'a', 'b', 'c', 'd', 'e', '\0'};
    
    cout << S1 << endl;
    
    run_reverse(S1);

    cout << S1 << endl;
}
