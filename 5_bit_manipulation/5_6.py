#! /usr/bin/env python3

# swap odd and even bits in int with as few instructions as possible, 
# i.e. 0 and 1, swapped, 2 and 3, etc...

def swap_even_odd_bits(num):

    mask = 0xAAAAAAAA 
    
    evens = mask & num
    evens >>= 1

    odds = (mask >> 1) & num
    odds <<= 1

    return  evens ^ odds


num = 9038171
print(bin(num))

res = swap_even_odd_bits(num)

s1 = str(bin(num))[2:]
s2 = str(bin(res))[2:]

if len(s2) < len(s1):
    s2 = "0" + s2
elif len(s1) < len(s2):
    s1 = "0" + s1

print(s1)
print(s2)



