#! /usr/bin/env python3


# number of bits required to convert int A to int B?
# eg 31, 14 -> output 2


def diff_bin(a, b):

    mask = 0x1
    bins_diff = 0

    while a or b:
        if a & mask != b & mask:
            bins_diff += 1
        a >>= 1
        b >>= 1

    return bins_diff

#a, b = 31, 14

a = 0b11110101
b =  0b1000100


print(bin(a), bin(b))

df = diff_bin(a, b)
print(df)

