#! /usr/bin/env python3

# print bin repr of double between 0 and 1
# if 32 bits not even to accurately show it, print ERROR

e = 1e-12

def bin_repr(num):

    #import pdb; pdb.set_trace()

    out = ""
    for i in range(1,33):
        
        next_power = 1.0/(pow(2, i))

        #print("num:", num, "np:", next_power, out)

        if next_power > num:
            out += "0"
        else:
            out += "1"
            num -= next_power 
            if abs(num) < e:
                return out, "OK"
   
    return out, "ERROR"

nums = [0.72, 0.5, 0.75, 0.999]

for num in nums:
    res, status = bin_repr(num)
    print(num, res, status) 
