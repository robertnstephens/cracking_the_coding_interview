#! /usr/bin/env python3

# what does ((n & (n-1)) == 0) do?

# -> it means n is a power of 2


n = pow(2, 10)

print( n & (n-1))




