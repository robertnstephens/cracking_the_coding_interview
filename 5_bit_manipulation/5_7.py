#! /usr/bin/env python3

# find missing int in O(n) time
# can only access jth bit of ith int, not whole int


# sorted?

A = [i for i in range(200)]

del A[120]

cur_bit = 0
mask = 0x1
for a in A:
    if cur_bit != a & mask:
        print(a-1)
        break

    cur_bit +=1 
    cur_bit %= 2
