#! /usr/bin/env python3

# given positive integer, print next smallest and next largest number with same number of 1s in binary?


# 1100 -> 1010, 10001


def next_biggest(num):
    # move the least significant 1 we can to left
    # (so first one with a 0 in front)

    mask = 0x1
    while not mask & num:
        mask <<= 1

    one_block_count = 0
    while mask & num:
        mask <<= 1
        one_block_count += 1
    
    num |= mask
    num = num & ~(mask-1)
    num |= (1 << one_block_count-1)-1

    return num


def next_smallest(num):
    # move the least significant 1 we can to right
    # (so first one with a 0 in behind)

    #import pdb; pdb.set_trace()

    mask = 0x1
    one_block_count, zero_block_count = 0, 0
    
    while mask & num:
        mask <<= 1
        one_block_count += 1

    while not mask & num:
        mask <<= 1
        zero_block_count += 1
    
    num = num & ~((mask<<1)-1)
    mask >>= 1
    num |= mask
    
    replacement_ones = (1 << one_block_count)-1
    
    if zero_block_count-1 < 0:
        print("error, can't make smaller num with same ones")
        return None
    
    replacement_ones <<= zero_block_count-1
    num |= replacement_ones
    
    return num

def next_biggest_smallest(num):
    next_big = next_biggest(num)
    next_small = next_smallest(num)

    return next_big, next_small

'''
n = 0b1001100
m = 0b1010001 # -> next biggest
p = 0b1001010 # -> next smallest
print(bin(n))
print(bin(m))
print(bin(p))
a, b = next_biggest_smallest(n)
print(bin(a), bin(b))
'''

import sys
n = int(sys.argv[1])
print(bin(n))
a, b = next_biggest_smallest(n)
print(bin(a), bin(b))



