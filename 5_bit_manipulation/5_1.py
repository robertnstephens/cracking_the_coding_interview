#! /usr/bin/env python3

# insert M into N such that M starts at bit j and ends at bit i
# j though i have enough bits to store M

# input:  N 1000000000, M = 10011, i = 2, j = 6
# output: N 1000100110

# zero i -> j
# shift M by i
# N | M


def insert_m_n(M, N, i, j):

    mask = 0x1

    mask <<= (j+1-i)
    mask -= 1
    mask <<= i

    N = N & ~mask
    N = N | M<<i
    return N
    

N = 0b1000100000
M = 0b10011
i = 2
j = 6


print(bin(N))
print(bin(M))
print(i, j)

res = insert_m_n(M, N, i, j)
print(bin(res))
