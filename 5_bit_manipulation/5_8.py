#! /usr/bin/env python3

# 8 pixels stored per byte
# width w, w % 8 == 0

import math

# draw line from (x1, y) to (x2, y)
def draw_horizontal_line(screen, width, x1, x2, y):

    row = y*width

    start_full_byte = row+math.ceil(x1/8)
    end_full_byte = row+math.floor(x2/8)

    start_bits = 8-x1%8
    end_bits = x2%8

    for group in range(start_full_byte, end_full_byte):
        screen[group] = 0xFF

    screen[start_full_byte-1] |= (1 << start_bits)-1
    screen[end_full_byte] |= ((1 << end_bits)-1)<<8-end_bits

def print_screen(screen, width):
    
    print("#"*width*8)
    out = ""
    for i, row in enumerate(screen):
        if i % width == 0:
            print(out)
            out = ""
        pixels = str(bin(row))[2:]
        pixels = (8-(len(pixels)))*"0" + pixels
        out += pixels 

    print("#"*width*8)

width = 4
height = 10
screen = [0x00 for _ in range(width*height)]

y = 1
x1, x2 = 1, 17

print_screen(screen, width)

draw_horizontal_line(screen, width, x1, x2, y)

print_screen(screen, width)
