#include <cstdlib>
#include <iostream>

// find the max of two numbers without using if-else or any other comparison operator

using namespace std;


bool is_pos(int n) {
    int a = ((1 << 31) & n);
    return !static_cast<bool>(a);
}

int find_max(int a, int b) {
    bool sign = is_pos(a - b);
    return sign*a + (!sign)*b;
}

int main(void) {

    cout << "find max using no comparison operators" << endl;

    int a = 10;
    int b = 5;

    cout << "max of " << a << ", " << b << ": " << find_max(a, b) << endl;

    a = 10;
    b = 25;
    cout << "max of " << a << ", " << b << ": " << find_max(a, b) << endl;
    
    return 0;
}

