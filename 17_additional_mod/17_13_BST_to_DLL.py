#! /usr/bin/env python3

# use class which can represent both binary search tree and doubly linked list

# write function which can convert BST into DLL in order and in place


class BiNode():
    def __init__(self, val):
        self.val = val
        self.node1 = None
        self.node2 = None

def concate_dll(ll1, ll2):
    ll1.node2, ll2.node1 = ll2, ll1

def convert_bst_to_dll(cur_node):
    if cur_node == None:
        return None, None

    left_head, left_tail = convert_bst_to_dll(cur_node.node1)
    right_head, right_tail = convert_bst_to_dll(cur_node.node2)
  
    new_head, new_tail = cur_node, cur_node

    if left_tail != None:
        concate_dll(left_tail, cur_node)
        new_head = left_head

    if right_head != None: 
        concate_dll(cur_node, right_head)
        new_tail = right_tail
   
    return (new_head, new_tail)


def traverse_bst_in_order(cur_node):
    if cur_node.node1 != None:
        traverse_bst_in_order(cur_node.node1)
    
    print(cur_node.val) 

    if cur_node.node2 != None:
        traverse_bst_in_order(cur_node.node2)


root = BiNode(10)
root.node1 = BiNode(5)
root.node1.node1 = BiNode(2)
root.node1.node2 = BiNode(7)
root.node2 = BiNode(15)
root.node2.node1 = BiNode(12)
root.node2.node2 = BiNode(19)

print("#"*10, "traverse BST in order:", "#"*10)
traverse_bst_in_order(root)
print("#"*40)

#import pdb;pdb.set_trace()

head, tail = convert_bst_to_dll(root)


print("#"*10, "traverse DLL forwards:", "#"*10)
cur_node = head
while cur_node != tail:
    print(cur_node.val)
    cur_node = cur_node.node2
print(cur_node.val)
print("#"*40)

print("#"*10, "traverse DLL forwards:", "#"*10)
cur_node = tail
while cur_node != head:
    print(cur_node.val)
    cur_node = cur_node.node1
print(cur_node.val)
print("#"*40)


