#! /usr/bin/env python3

# find all pairs of ints within array that sum to specified value

import random

# assumes all values are distinct
def all_sum_pairs(A, target):
    A.sort()
    s, e = 0, len(A)-1
    sum_pairs = []

    while s < e:
        if A[s] + A[e] == target:
            sum_pairs.append( (A[s], A[e]) )
            s += 1 
            e -= 1 
        elif A[s] + A[e] < target:
            s += 1
        else:
            e -= 1

    return sum_pairs

A = random.sample(range(-50, 50), 30)

target = 10
res = all_sum_pairs(A, target)

print(A)
print("target:", target)
for rs in res:
    print(rs)


