#! /usr/bin/env python3

# find contiguous sequence with largest sum - return this sum

# use Kadane's algorithm


def kadane(A):

    max_so_far = cur_max = 0

    for a in A:
        cur_max = max(cur_max + a, 0)
        max_so_far = max(cur_max, max_so_far)

    return max_so_far

A = [2, -8, 3, -2, 4, -10]


res = kadane(A)

print(A)
print(res)

