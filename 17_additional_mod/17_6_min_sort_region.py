#! /usr/bin/env python3

'''
find incides m and n such that sorting elements m through n is sufficient to sort entire array
minimize n-m
eg:
A = [1, 2, 4, 7, 10, 11, 7, 12, 6, 7, 16, 18, 19]
output (3, 9)
'''

def min_sort_region(A):

    m, n = 0, len(A)-1

    for i in range(0, len(A)-1):
        if A[i] > min(A[i+1:]):
            m = i
            break
           
        if A[i] == min(A[i+1:]):
            while A[i] == A[i+1]:
                i += 1
            m = i
            break

    for i in range(len(A)-1, 2, -1):
        if A[i] < max(A[:i-1]):
            n = i
            break

        if A[i] == max(A[:i-1]):
            while A[i] == A[i-1]:
                i -= 1
            n = i
            break

    return m, n

A = [1, 2, 4, 7, 10, 11, 7, 12, 6, 7, 16, 18, 19]
B = [1, 2, 4, 7, 10, 11, 7, 12, 7, 7, 16, 18, 19]
C = [1, 2, 4, 7, 7,  11, 7, 12, 7, 7, 16, 18, 19]
D = [1, 5, 2, 9, 9, 9, 9, 10]

m, n = min_sort_region(A)
print(m, n)

m, n = min_sort_region(B)
print(m, n)

m, n = min_sort_region(C)
print(m, n)

m, n = min_sort_region(D)
print(m, n)
