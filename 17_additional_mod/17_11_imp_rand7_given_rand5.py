#! /usr/bin/env python3

import random

# implement rand7() (0 .. 6 inc) given rand5() (0 .. 4 inc)


# we're "given" this
def rand5():
    return random.randint(0, 4) 

def rand7():
    while 1:
        res = 5*rand5() + rand5()
        if 0 <= res < 21:
            return res % 7

freq_map = {i:0.0 for i in range(7)}
count = 100000

for _ in range(count*7):
    freq_map[rand7()] += 1

for k in freq_map:
    print('{}: {}%'.format(k, freq_map[k]*100.0/count))

