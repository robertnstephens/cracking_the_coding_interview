#! /usr/bin/env python3

# n philosophers, n chopsticks
# always pick left hand chop stick first
# avoid deadlock

# solution -> if cannot pick up right chopstick, put down left

import threading, time

class ChopStick():
    def __init__(self, csid):
        self.lock = threading.Lock()
        self.csid = csid 

    def pick_up(self):
        if self.lock.locked():
            return False
        else:
            self.lock.acquire()
            return True 

    def put_down(self):
        self.lock.release()


class Philosopher():
    def __init__(self, phil_id, left_chopstick, right_chopstick):
        self.left = left_chopstick
        self.right = right_chopstick
        self.bites = 5
        self.phil_id = phil_id

    def pick_up(self):
        if not self.left.pick_up():
            return False

        if not self.right.pick_up():
            self.left.put_down()
            return False
        
        return True

    def put_down(self):
        self.left.put_down()
        self.right.put_down()

    def chew(self):
        print("philosopher {} chews bite {}".format(self.phil_id, self.bites_so_far))
        time.sleep(0.5)

    def one_bite(self):
        if self.pick_up():
            self.chew()
            self.put_down()
            return True
        else:
            return False

    def eat(self):
        self.bites_so_far = 0 

        while self.bites_so_far < self.bites:
            if self.one_bite():
                self.bites_so_far += 1


chopsticks = [ChopStick(i) for i in range(10)]

philosophers = [Philosopher(i, chopsticks[i], chopsticks[i+1]) for i in range(len(chopsticks)-1)]
philosophers.append(Philosopher(len(chopsticks)-1, chopsticks[-1], chopsticks[0]))


threads = [threading.Thread(target=philosopher.eat) for philosopher in philosophers]

for thread in threads:
    thread.start()

