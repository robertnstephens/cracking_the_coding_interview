#! /usr/bin/env python3

import threading, time, random


# for one single instance of Runner passed to three different threads,
# ensure first() is always run before second() which is always run before third

class Runner():
    def __init__(self, e1, e2):
        self.e1 = e1 
        self.e2 = e2

    def first(self):
        print("run first...")
        self.e1.set()

    def second(self):
        event_is_set = self.e1.wait()
        if event_is_set:
            print("run second...")
            e2.set()

    def third(self):
        event_is_set = self.e2.wait()
        if event_is_set:
            print("run third...")
            self.e1.clear()
            self.e2.clear()

e1 = threading.Event()
e2 = threading.Event()
runner = Runner(e1, e2)

t1 = threading.Thread(name="t1", target=runner.first)
t2 = threading.Thread(name="t2", target=runner.second)
t3 = threading.Thread(name="t3", target=runner.third)

threads = [t1, t2, t3]
random.shuffle(threads)

for t in threads:
    t.start()


