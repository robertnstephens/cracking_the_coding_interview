#! /usr/bin/env python3

'''
measure time spent in context switches
'''

# TODO: this code follows the english language answer in the book but the times it yields don't make sense


from multiprocessing import Process, Pipe, Manager
import time


def pingpong(endpoint, message, return_dict):

    idx, lim = 0, 100000
    min_time = 1000*1000
    av_time = 0.0

    while idx < 100000:
        t1 = time.time()
        endpoint.send([idx, message])
        res = endpoint.recv()
        t2 = time.time() 
   
        #t2 - t1 = 2*(send_time + rec_time + context_switch_time)
        if min_time > t2-t1:
            min_time = t2-t1
            print(min_time, idx)

        av_time += t2-t1
        idx += 1
   
    return_dict[message + "MIN"] = min_time
    return_dict[message + "AV"] = av_time/lim

    endpoint.close()


def pingpong_single_proc(endpoint1, endpoint2):

    idx, lim = 0, 100000

    min_time = 1000*1000
    av_time = 0.0

    while idx < lim:
        t1 = time.time()
        endpoint1.send([idx, "1->2"])
        endpoint2.recv() 
        endpoint2.send([idx, "2->1"])
        res = endpoint1.recv()
        t2 = time.time() 
   
        #t2 - t1 = 2*(send_time + rec_time + context_switch_time)
        if min_time > t2-t1:
            min_time = t2-t1
            print(min_time, idx)

        av_time += t2-t1
        idx += 1
    
    endpoint1.close()
    endpoint2.close()

    return min_time, av_time/lim

if __name__ == '__main__':


    manager = Manager()
    return_dict = manager.dict()

    endpoint1, endpoint2 = Pipe()

    mess1, mess2 = "send 1->2, recv 1<-2", "send 2->1, recv 2<-1"

    proc1 = Process(target=pingpong, args=(endpoint1, mess1, return_dict))
    proc2 = Process(target=pingpong, args=(endpoint2, mess2, return_dict))


    print("calculate send + rec + context switch time by sending between processes:")

    proc1.start()
    proc2.start()

    proc1.join()
    proc2.join()

    min_inter_proc_time = min(return_dict[mess1+"MIN"], return_dict[mess2+"MIN"])
    av_inter_proc_time = (return_dict[mess1+"AV"] + return_dict[mess2+"AV"])/2
    
    print("min_inter_proc_time:", min_inter_proc_time)
    print("av_inter_proc_time:", av_inter_proc_time)

    print("now calculate send and rec time by sending from one process to itself:")
    endpoint1, endpoint2 = Pipe()
    min_single_proc_time, av_single_proc_time = pingpong_single_proc(endpoint1, endpoint2)
    print("min_single_proc_time:", min_single_proc_time)
    print("av_single_proc_time:", av_single_proc_time)

    context_switch_time = (min_inter_proc_time - min_single_proc_time) / 2
    print("min context switch time:", context_switch_time)
    
    av_context_switch_time = (av_inter_proc_time - av_single_proc_time) / 2
    print("average context switch time == {} ms".format(av_context_switch_time*1000))



