#! /usr/bin/env python3

# write a function which returns all subsets of a set

# (the set of all subsets of a set is called a power set)

def power_set_rec(A, n):
    if n == len(A):
        return [[]]

    subsets = power_set_rec(A, n+1)
    new_subsets = []
    new_subsets += subsets

    for sb in subsets:
        e = sb + [A[n]]
        new_subsets.append(e)

    return new_subsets

A = [1,2,3]

res = power_set_rec(A, 0)
print(res)

