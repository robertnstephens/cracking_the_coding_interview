#! /usr/bin/env python3


# paint_fill() function
# given a point, a screen (2D array), a new colour, replace old colour


'''
11000
00010
01111
00011

paint_fill(1,2,K)

11OOO
OOO2O
O2222
OOO22
'''

S = [[1,1,0,0,0],
     [0,0,0,1,0],
     [0,1,1,1,1],
     [0,0,0,1,1]]

print("#"*25)
for row in S:
    print(row)


def paint_fill(S, x, y, oc, nc):

    if x < 0 or x >= len(S[0]) or y < 0 or y >= len(S) or S[y][x] != oc:
        return None
   
    S[y][x] = nc
   
    paint_fill(S, x-1, y, oc, nc)
    paint_fill(S, x+1, y, oc, nc)
    paint_fill(S, x, y-1, oc, nc)
    paint_fill(S, x, y+1, oc, nc)
        

def paint_fill_wrap(S, x, y, nc):
    oc = S[y][x] 
    return paint_fill(S, x, y, oc, nc)

x, y = 1, 2
nc = 2

paint_fill_wrap(S, x, y, nc)

print("#"*25)
for row in S:
    print(row)


x, y = 1, 0
nc = 3

paint_fill_wrap(S, x, y, nc)

print("#"*25)
for row in S:
    print(row)
print("#"*25)

