#! /usr/bin/env python3

import math

# start at (0, 0), can only move right or down each step
# how many ways to go from (0, 0) to (X, Y)?


X, Y = 4, 5

# Answer 1 - number of ways of picking X "downward" moves from X+Y
# A choose B is A!/((A-B)!B!)

res = math.factorial(X+Y)//(math.factorial(X)*math.factorial(Y))
print("mathematical:", X, Y, res)


# Answer 2 - recursively

def num_ways(x, y):
    if x == 0 and y == 0:
        return 1
    elif x < 0 or y < 0:
        return 0
    else:
        return num_ways(x-1, y) + num_ways(x, y-1)

res = num_ways(X, Y)
print("recursive:", X, Y, res)


# what if certain spots are off limits?

off_limits = [[1,2], [2,2], [3,4]]

def num_ways(x, y, off_limits):
    if x == 0 and y == 0:
        return 1
    elif x < 0 or y < 0 or [x, y] in off_limits:
        return 0
    else:
        return num_ways(x-1, y, off_limits) + num_ways(x, y-1, off_limits)

res = num_ways(X, Y, off_limits)
print("recursive:", X, Y, res)

