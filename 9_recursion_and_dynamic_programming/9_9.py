#! /usr/bin/env python3

import copy

# print all ways of arranging 8 queens on 8*8 chessboard
# such that no two share same row, column or diagonal

BOARD_SIZE = 8

possible_boards = 0


def print_board(board):
    print("#"*25)
    for row in board:
        print(row)
    print("#"*25)

    global possible_boards
    possible_boards += 1

def arrange_queens(board, row):
    if row == BOARD_SIZE:
        print_board(board)
        return    

    for col in range(BOARD_SIZE):
        if board[row][col] == 0:
            cur_board = copy.deepcopy(board)
            cur_board[row][col] = 1
   
            # column below now off limits
            for r in range(row+1, BOARD_SIZE):
                cur_board[r][col] = 2
    
            # left diagonal
            r, c = row+1, col-1
            while r < BOARD_SIZE and c >= 0:
                cur_board[r][c] = 2
                r += 1
                c -= 1

            # right diagonal
            r, c = row+1, col+1
            while r < BOARD_SIZE and c < BOARD_SIZE:
                cur_board[r][c] = 2
                r += 1
                c += 1

            arrange_queens(cur_board, row+1)


board = [[0 for _ in range(BOARD_SIZE)] for _ in range(BOARD_SIZE)]

arrange_queens(board, 0)
print("possible_boards:", possible_boards)



