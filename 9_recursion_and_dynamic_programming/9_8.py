#! /usr/bin/env python3

# given infinite 25, 10, 5, 1 coins calc number ways representing n cents


# so presumably, for n = 6, 1 + 5 is different than 5 + 1


def calc_ways(n, last_coin):
    coins = [25, 10, 5, 1]

    if n == 0:
        return 1
    elif n < 0:
        return 0

    ways = 0
    for i in range(last_coin, len(coins)):
        ways += calc_ways(n-coins[i], i)

    return ways

n = 6
res = calc_ways(n, 0)
print(n, "=> should be 2: [5,1], [1,1,1,1,1,1]")
print(res)

n = 10
res = calc_ways(n, 0)
print(n, "=> should be 4: [10], [5,5], [5,1,1,1,1,1], [1,1,1,1,1,1,1,1,1,1]")
print(res)

