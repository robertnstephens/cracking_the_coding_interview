#! /usr/bin/env python3

# obtain all permutations of a string

def get_perms_rec(A, n):
    if n == len(A)-1:
        return [[A[n]]]

    perms = get_perms_rec(A, n+1)
   
    new_perms = []

    for perm in perms:
        for i in range(len(perm)+1):
            new_perms.append( perm[:i] + [A[n]] + perm[i:] )

    return new_perms

S = "abcd"
A = [c for c in S]

res = get_perms_rec(A, 0)

for p in res:
    print( ''.join(p))

