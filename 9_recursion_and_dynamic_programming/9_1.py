#! /usr/bin/env python3

# a child runs up n steps, either 1, 2 or 3 at a time
# how many possible ways can it do so?


def count_ways(n, dp_cache):
    if n == 0:
        return 1
    elif n < 0:
        return 0
    else:
        ways = 0
        for i in range(1, 4):
            if dp_cache.get(n-i) == None:
                dp_cache[n-i] = count_ways(n-i, dp_cache)
            ways += dp_cache[n-i]

    return ways

n = 40
dp_cache = {}
res = count_ways(n, dp_cache)
print("steps:", n, "ways:", res)


