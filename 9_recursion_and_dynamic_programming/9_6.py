#! /usr/bin/env python3

# print all valid n-pairs of parentheses

# TODO: accomplish without some dups
def par_rec(n):
    if n == 1:
        return [["(", ")"]]

    parens = par_rec(n-1)
    new_parens = []

    for par in parens:
        for i in range(len(par)//2 + 1):
            e = par[:i] + ["(", ")"] + par[i:]            
            new_parens.append(e)

    return new_parens

res = par_rec(3)

res = set([''.join(e) for e in res])

for e in res:
    print(e)

