#! /usr/bin/env python3


# given boolean expression consisting of 0, 1, &, | and ^ count
# number of ways of evaluating to desired result

# eg, desired result = False:

'''
1^0|0|1 =>

1^(0|(0|1))
1^((0|0)|1)

output therefore two ways
'''


def evaluate(exp): 
    # error checking 
    if len(exp) != 3:
        print("len exp error:", len(exp), exp)
        return 
    if exp[1] not in ["&", "|", "^"]:
        print("operator not in middle of expression:", exp)
        return 
    if exp[0] not in ["0","1"] or exp[2] not in ["0","1"]:
        print("start and/or end not booleans", exp)
        return 

    # evaluation 
    if exp[1] == "&":
        if exp[0] == "1" and exp[2] == "1":
            return "1"
        else:
            return "0"
    elif exp[1] == "|":
        if exp[0] == "1" or exp[2] == "1":
            return "1"
        else:
            return "0"
    elif exp[1] == "^":
        if exp[0] == "1" and exp[2] == "0":
            return "1"
        elif exp[0] == "0" and exp[2] == "1":
            return "1"
        else:
            return "0"

def num_ways_parens(des, exp):

    # our base case
    if len(exp) == 3:
        return des == int(evaluate(exp))

    ways = 0
    for i in range(0, len(exp)-2, 2):
        new_exp = exp[:i] + evaluate(exp[i:i+3]) + exp[i+3:]
        ways += num_ways_parens(des, new_exp)

    return ways


expression = "1^0|0|1"
desired_result = 0
ways = num_ways_parens(desired_result, expression)
print(expression, desired_result, ways)


expression = "1^0|0|1&1^0"
desired_result = 0
ways = num_ways_parens(desired_result, expression)
print(expression, desired_result, ways)


expression = "1^0&1"
desired_result = 1
ways = num_ways_parens(desired_result, expression)
print(expression, desired_result, ways)


expression = "0|0|0^0&0"
desired_result = 1
ways = num_ways_parens(desired_result, expression)
print(expression, desired_result, ways)

