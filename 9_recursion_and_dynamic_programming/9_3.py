#! /usr/bin/env python3

import random

# find "magic index" such that A[n] = n (if such exists) in sorted array

# follow up - what if numbers not unique?

A = [-10, -5, -2, 3, 10, 20, 30, 50, 60]

A.sort()
print(A)


def find_magic_index_rec(A, low, high):
    if low > high:
        return None
    mid = (low+high)//2
    if A[mid] == mid:
        return mid
    elif A[mid] < mid:
        return find_magic_index_rec(A, mid+1, high)
    else:
        return find_magic_index_rec(A, low, mid-1)

    return None

def find_magic_index(A):
    res = find_magic_index_rec(A, 0, len(A)-1)

    if res != None:
        print(res)
    else:
        print("no magic index")

find_magic_index(A)

###########################################
# follow up - what if numbers not unique? #
###########################################

def find_magic_index_rec_dups(A, low, high):
    if low > high:
        return None
    mid = (low+high)//2

    mid_l, mid_r = mid, mid

    while A[mid_r] == A[mid]:
        if A[mid_r] == mid_r:
            return mid_r
        mid_r += 1

    while A[mid_l] == A[mid]:
        if A[mid_l] == mid_l:
            return mid_l
        mid_l -= 1

    if A[mid_l] < mid:
        return find_magic_index_rec_dups(A, mid_l, high)
    else:
        return find_magic_index_rec_dups(A, low, mid_r)

    return None

def find_magic_index_dups(A):
    res = find_magic_index_rec_dups(A, 0, len(A)-1)

    if res != None:
        print(res)
    else:
        print("no magic index")

A = [-10, -5, -2, 3, 3, 3, 3, 3, 10, 20, 30, 50, 60, 60, 60]
print(A)
find_magic_index_dups(A)


