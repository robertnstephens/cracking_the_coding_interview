#! /usr/bin/env python3

import copy


# n boxes, each width wi, height hi, depth di
# obtain tallest stack possible - boxes can only be placed on top of each other if:
# wi+1 < wi, hi+1 < hi, di+1 < di


# max stack is max of stacks starting with b0, b1, b ...
# max stack starting with b0 is max of stacks [b0, b1 ... ], [b0, b2 ...]


class Box(object):
    def __init__(self, w, h, d):
        self.w = w
        self.h = h
        self.d = d
    
    # if this box can be placed above one passed in
    def can_be_above(self, in_box):
        return self.w < in_box.w and self.h < in_box.h and self.d < in_box.d

def calc_height(stacked_boxes):
    return sum([box.h for box in stacked_boxes])

# this one is based on book - DOESN'T WORK THOUGH!!!
def create_stack_rec(boxes, bottom):
    max_height = 0
    max_stack = None

    for cur_box in boxes:
        if cur_box.can_be_above(bottom):
            new_stack = create_stack_rec(boxes, cur_box)
            new_height = calc_height(new_stack)

            if new_height > max_height:
                max_height = new_height
                max_stack = new_stack

    if max_stack == None:     
        max_stack = []

    if bottom != None:
        max_stack.insert(0, bottom)

    return max_stack


def create_stack_dp(boxes, bottom, stack_map):
   
    if bottom in stack_map:
        return stack_map[bottom]

    max_height = 0
    max_stack = None

    for cur_box in boxes:
        if cur_box.can_be_above(bottom):
            new_stack = create_stack_dp(boxes, cur_box, stack_map)
            new_height = calc_height(new_stack)

            if new_height > max_height:
                max_height = new_height
                max_stack = new_stack

    if max_stack == None:     
        max_stack = []

    if bottom != None:
        max_stack.insert(0, bottom)

    stack_map[bottom] = max_stack

    return copy.deepcopy(max_stack)




box0 = Box(10, 9, 8)
box1 = Box(9, 8, 7)
box2 = Box(8, 7, 6)
boxes = [box0, box1, box2]

max_stack = create_stack_rec(boxes, boxes[0])

print("#"*25)
for box in max_stack:
    print(box.h)
print("#"*25)

box0 = Box(5, 9, 8)
box1 = Box(9, 8, 7)
box2 = Box(8, 7, 6)
box3 = Box(4, 4, 4)

boxes = [box0, box1, box2, box3]

max_stack = create_stack_rec(boxes, boxes[0])

print("#"*25)
for box in max_stack:
    print(box.h)
print("#"*25)


max_stack = create_stack_dp(boxes, boxes[0], {})

print("#"*25)
for box in max_stack:
    print(box.h)
print("#"*25)




