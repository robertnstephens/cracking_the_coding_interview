#! /usr/bin/env python3

import LL

if __name__ == "__main__":

    head = LL.Node(0)
    for i in range(1, 10):
        head.append_to_tail( LL.Node(i) )
    head.print_vals()

    print("delete 4th:")
    head = head.delete_node(4)
    head.print_vals()

    print("delete head:")
    head = head.delete_node(0)
    head.print_vals()

    print("delete tail:")
    head = head.delete_node(9)
    head.print_vals()


    output = "reset:"
    print(output) 
    head = LL.Node(0)
    for i in range(1, 10):
        head.append_to_tail( LL.Node(i) )
    head.print_vals()


    output = "turn a1, a2 ... an, b1, b2 ... bn into a1, b1, a2, b2 ... an, bn:"
    print(output) 

    p1, p2 = head, head

    while p1 != None and p1.next != None:
        p1 = p1.next.next
        p2 = p2.next
    # now p2 is at mid point
    p1 = head
    while p2 != None:
        p1 = p1.insert_node_after(p2.val)
        if p2.next == None:
            p1.next = None
        p1 = p1.next
        p2 = p2.next

    head.print_vals()
