#! /usr/bin/env python3

# partition linked list around some value x
# all nodes less than value come before, all greater than or equal after

import random
import LL

vals = [random.randint(0, 100) for _ in range(10)]

head = LL.Node(vals[0])
for i in range(1, len(vals)):
    head.append_to_tail(LL.Node(vals[i]))
head.print_vals()

x = 50
print("partition val, x:", x)

# calculate size so no infinite adding to tail happens
cur = head
count = 0
while cur != None:
    count += 1
    cur = cur.next

cur = head
# find tail
cur = head
while cur.next != None:
    cur = cur.next
tail = cur

# want to just do one pass - keep track of current tail and current examined node (happens in LL.py)
cur = head
i = 0
#import pdb
#pdb.set_trace()

while cur != None and i < count:
    temp_next = cur.next
    if cur.val >= x:
        if cur == head:
            head = cur.next
        # remove cur from old position
        #cur.delete_this_node()
        if cur.prev != None:
            cur.prev.next = cur.next
        if cur.next != None: 
            cur.next.prev = cur.prev

        # place cur on tail
        tail.next = cur
        cur.prev = tail
        cur.next = None

        # repoint tail
        tail = tail.next

    cur = temp_next
    i += 1

head.print_vals()
