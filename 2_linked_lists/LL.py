class Node(object):

    def __init__(self, val):
        self.val = val
        self.prev = None
        self.next = None
        self.tail = None

    def append_to_tail(self, node):
        node.next = None
        if self.tail != None and self.tail.next == None:
            self.tail.next = node
            node.prev = self.tail       
        else:
            cur = self
            while cur.next != None:
                cur = cur.next
            node.prev = cur
            cur.next = node
        self.tail = node

    def delete_node(self, val):
        if self.val == val:
            if self.next != None:
                self.next.prev = None
                return self.next
            else:
                return None
        cur = self
        while cur.next != None:
            cur = cur.next
            if cur.val == val:
                cur.prev.next = cur.next
                if cur.next != None: 
                    cur.next.prev = cur.prev 
                return self
        return None

    def delete_this_node(self):
        if self.prev != None:
            self.prev.next = self.next
        if self.next != None:
            self.next.prev = self.prev

    def insert_node_after(self, val):
        new_node = Node(val)
        if self.next != None:
            new_node.next = self.next
            self.next.prev = new_node 

        self.next = new_node
        new_node.prev = self
        return self.next

    def size(self):
        count, cur = 0, self
        while cur:
            count += 1
            cur = cur.next
        return count

    def print_node(self, cur):
        output = ""
        if cur != None:
            output += "cur:" + str(cur.val) + ", "

        if cur.prev != None:
            output += "prev:" + str(cur.prev.val) + ", "

        if cur.next != None:
            output += "next:" + str(cur.next.val)
        print(output)

    def print_vals(self):
        print("#"*25) 
        cur = self        
        while cur != None:
            self.print_node(cur)
            cur = cur.next
        print("#"*25) 
