#! /usr/bin/env python3


# input: (7->1->6) + (5->9->2) = 617 + 295 = 912, i.e. output: (2->1->9)
# what if digits stored in reverse order, i.e. most significant first?

import LL

h1 = LL.Node(7)
h1.append_to_tail(LL.Node(1))
h1.append_to_tail(LL.Node(6))
h1.append_to_tail(LL.Node(9))

h2 = LL.Node(5)
h2.append_to_tail(LL.Node(9))
h2.append_to_tail(LL.Node(2))

'''
h1 = LL.Node(9)
h1.append_to_tail(LL.Node(9))
h1.append_to_tail(LL.Node(9))
h1.append_to_tail(LL.Node(9))

h2 = LL.Node(9)
h2.append_to_tail(LL.Node(9))
h2.append_to_tail(LL.Node(9))
'''

h1.print_vals()
h2.print_vals()

h_output = None
c1, c2 = h1, h2

carry = 0
while c1 and c2:
    sum_vals = carry + (c1.val+c2.val)
    if sum_vals >= 10:
        sum_vals = sum_vals%10
        carry = 1
    else:
        carry = 0

    if h_output == None:
        h_output = LL.Node(sum_vals)
    else:
        h_output.append_to_tail(LL.Node(sum_vals))

    c1 = c1.next
    c2 = c2.next

def last_digits(c, carry, h_output):
    while c:
        if carry+c.val == 10: 
            h_output.append_to_tail(LL.Node(0))
            h_output.append_to_tail(LL.Node(1))
        else: 
            h_output.append_to_tail(LL.Node(carry+c.val))
        carry = 0
        c = c.next

if c1:
    last_digits(c1, carry, h_output)
elif c2:
    last_digits(c2, carry, h_output)
else:
    pass

h_output.print_vals()


# reverse order -> better to use doubly linked list and just start at tails instead of heads

# assuming I can't:

print("reverse order:")

h1 = LL.Node(7)
h1.append_to_tail(LL.Node(1))
h1.append_to_tail(LL.Node(6))
h1.append_to_tail(LL.Node(9))

h2 = LL.Node(5)
h2.append_to_tail(LL.Node(9))
h2.append_to_tail(LL.Node(2))

h1.print_vals()
h2.print_vals()

h_output = None
c1, c2 = h1, h2

num_digits1 = h1.size()
num_digits2 = h2.size()

total1 = 0
while c1:
    total1 += c1.val * int(pow(10, num_digits1-1))
    num_digits1 -= 1
    c1 = c1.next

total2 = 0
while c2:
    total2 += c2.val * int(pow(10, num_digits2-1))
    num_digits2 -= 1
    c2 = c2.next

total_output = [int(c) for c in str(total1+total2)]

print("total1:", total1)
print("total2:", total2)
print("total_output:", total_output)

h_output = LL.Node(total_output[0])
for i in range(1, len(total_output)):
    h_output.append_to_tail(LL.Node(total_output[i]))

h_output.print_vals()



