#! /usr/bin/env python3

# find kth to last element of singly linked list
# how to do this recursively?

import LL


def kth_to_last(head, k):
    p1, p2 = head, head

    for _ in range(k):
        if p2 == None:
            print("error, list not long enough for kth ==", k)
            return
        p2 = p2.next

    while p2 != None:
        p1 = p1.next
        p2 = p2.next

    print(p1.val, k)

head = LL.Node(0)
for i in range(1, 100):
    head.append_to_tail(LL.Node(i))

kth_to_last(head, 1)
kth_to_last(head, 2)
kth_to_last(head, 3)
kth_to_last(head, 99)
kth_to_last(head, 100)
kth_to_last(head, 101) # should error
