#! /usr/bin/env python3

# remove duplicates from unsorted linked list
# how to solve problem if temporary buffer isn't allowed?
# -> best is O(n^2) time, O(1) space

import LL

def remove_duplicates(head):
    cur = head
    vals_seen = {}
    while cur != None:
        if vals_seen.get(cur.val) != None:
            print(cur.val) 
            cur.prev.delete_node(cur.val)    
        vals_seen[cur.val] = True
        cur = cur.next

def remove_duplicates_no_buffer(head):
    cur = head
    while cur != None:
        search = cur.next         
        while search != None:
            if cur.val == search.val:
                print(cur.val) 
                search.prev.delete_node(cur.val)    
            search = search.next
        cur = cur.next

head = LL.Node(1)
head.append_to_tail(LL.Node(3))
head.append_to_tail(LL.Node(4))
head.append_to_tail(LL.Node(5))
head.append_to_tail(LL.Node(3))
head.append_to_tail(LL.Node(4))
head.append_to_tail(LL.Node(7))
head.append_to_tail(LL.Node(3))
head.append_to_tail(LL.Node(4))
head.append_to_tail(LL.Node(9))

head.print_vals()
print("remove duplicates:")
remove_duplicates(head)
head.print_vals()



head = LL.Node(1)
head.append_to_tail(LL.Node(3))
head.append_to_tail(LL.Node(4))
head.append_to_tail(LL.Node(5))
head.append_to_tail(LL.Node(3))
head.append_to_tail(LL.Node(4))
head.append_to_tail(LL.Node(7))
head.append_to_tail(LL.Node(3))
head.append_to_tail(LL.Node(4))
head.append_to_tail(LL.Node(9))

head.print_vals()
print("remove duplicates no buffer:")
remove_duplicates_no_buffer(head)
head.print_vals()

