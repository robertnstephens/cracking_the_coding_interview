#! /usr/bin/env python3

# determine if singly linked list is a palindrome

import LL

head_pal = LL.Node(7)
head_pal.append_to_tail(LL.Node(1))
head_pal.append_to_tail(LL.Node(2))
head_pal.append_to_tail(LL.Node(6))
head_pal.append_to_tail(LL.Node(2))
head_pal.append_to_tail(LL.Node(1))
head_pal.append_to_tail(LL.Node(7))

head_pal_even = LL.Node(7)
head_pal_even.append_to_tail(LL.Node(1))
head_pal_even.append_to_tail(LL.Node(2))
head_pal_even.append_to_tail(LL.Node(2))
head_pal_even.append_to_tail(LL.Node(1))
head_pal_even.append_to_tail(LL.Node(7))

head_not_pal = LL.Node(7)
head_not_pal.append_to_tail(LL.Node(1))
head_not_pal.append_to_tail(LL.Node(6))
head_not_pal.append_to_tail(LL.Node(2))
head_not_pal.append_to_tail(LL.Node(7))

# assume we don't know size


def is_palindrome(head):
    p1, p2 = head, head
    stack = [p1.val]
    while p2 and p2.next:
        p2 = p2.next.next
        p1 = p1.next
        stack.append(p1.val)
    
    # if even number of nodes, discard one-past-middle
    if p2: 
        #print("odd sized")  
        pass
    else:
        #print("even sized")  
        stack.pop()

    while p1:
        #print(p1.val, stack[-1]) 
        if p1.val != stack.pop():
            return False
        p1 = p1.next

    return True

# 7126217
def is_palindrome_recurse(cur, length):

    if cur == None or length == 0:
        return (None, True)
    elif length == 1:
        return (cur.next, True)
    elif length == 2:
        return (cur.next.next, cur.val == cur.next.val)

    back_node, palin_res = is_palindrome_recurse(cur.next, length-2)

    if not back_node or not palin_res:
        return (back_node, palin_res)
    else:
        return (back_node.next, back_node.val == cur.val)


def is_palindrome_rec(head):
    node, res = is_palindrome_recurse(head, head.size())
    return res

print("iterative:")
print( is_palindrome(head_pal_even) )
print( is_palindrome(head_pal) )
print( is_palindrome(head_not_pal) )

print("\nrecursive:")
print( is_palindrome_rec(head_pal_even) )
print( is_palindrome_rec(head_pal) )
print( is_palindrome_rec(head_not_pal) )

import random, time

length = 500
#import sys; sys.setrecursionlimit(length)
h_vals = [random.randint(0, 100000) for _ in range(length)]
h_vals = h_vals + h_vals[::-1]

head_long = LL.Node(h_vals[0])
for i in range(1, len(h_vals)):
    head_long.append_to_tail(LL.Node(h_vals[i]))

t0 = time.time()
print("iterative:")
print( is_palindrome(head_long) )
t1 = time.time()
print(t1-t0)

t0 = time.time()
print("recursive:")
print( is_palindrome_rec(head_long) )
t1 = time.time()
print(t1-t0)
